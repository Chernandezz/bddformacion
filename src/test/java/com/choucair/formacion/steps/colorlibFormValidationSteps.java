package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.ColorlibFormValidationPage;

import net.thucydides.core.annotations.Step;

public class colorlibFormValidationSteps {
	
	ColorlibFormValidationPage  colorlibFormValidationPage;
	
	@Step	
	public void diligenciar_popup_datos_tabla(List<List<String>> data,int id) {
		colorlibFormValidationPage.required(data.get(id).get(0).trim());
		colorlibFormValidationPage.select_Sport(data.get(id).get(1).trim());
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(2).trim());
		colorlibFormValidationPage.Multiple_Select(data.get(id).get(3).trim());
		colorlibFormValidationPage.Url(data.get(id).get(4).trim());
		colorlibFormValidationPage.Email(data.get(id).get(5).trim());
		colorlibFormValidationPage.Password1(data.get(id).get(6).trim());
		colorlibFormValidationPage.Password2(data.get(id).get(7).trim());
		colorlibFormValidationPage.MiniSize(data.get(id).get(8).trim());
		colorlibFormValidationPage.MaxSize(data.get(id).get(9).trim());
		colorlibFormValidationPage.Numero(data.get(id).get(10).trim());
		colorlibFormValidationPage.Ip(data.get(id).get(11).trim());
		colorlibFormValidationPage.Fecha(data.get(id).get(12).trim());
		colorlibFormValidationPage.FechaEarlier(data.get(id).get(13).trim());
		colorlibFormValidationPage.validate();
		
	}
	@Step
	public void verificar_ingreso_datos_formulario_exitoso() {
		colorlibFormValidationPage.form_sin_errores();
	}
	@Step
	public void verificar_ingreso_datos_formulario_con_errores() {
		colorlibFormValidationPage.form_con_errores();
	}

}

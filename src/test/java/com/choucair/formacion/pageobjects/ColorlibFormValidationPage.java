package com.choucair.formacion.pageobjects;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;

public class ColorlibFormValidationPage extends PageObject {
//	Campo Required
	@FindBy(xpath="//*[@id='req']")	
	public WebElementFacade txtRequired;
//	Campo seleccion de deporte 1
	@FindBy(xpath="//*[@id='sport']")
	public WebElementFacade cmbSport1;
//	Campo seleccion Multiple de deporte 1
	@FindBy(xpath="//*[@id='sport2']")
	public WebElementFacade cmbSport2;		
//	Campo Url
	@FindBy(xpath="//*[@id='url1']")
	public WebElementFacade txtUrl;
//	Campo E-mail
	@FindBy(xpath="//*[@id='email1']")
	public WebElementFacade txtEmail;
//	Campo Password 1
	@FindBy(xpath="//*[@id='pass1']")
	public WebElementFacade txtPass1;
//	Campo confirmar Password 1
	@FindBy(xpath="//*[@id='pass2']")
	public WebElementFacade txtPass2;
//	Campo Minimum field size (6)
	@FindBy(xpath="//*[@id='minsize1']")
	public WebElementFacade txtMinimun6;
//	Campo Maximum field size, optional
	@FindBy(xpath="//*[@id='maxsize1']")
	public WebElementFacade txtMaximunSize;
//	Campo Number
	@FindBy(xpath="//*[@id='number2']")
	public WebElementFacade txtNumber;
//	Campo IP
	@FindBy(xpath="//*[@id='ip']")
	public WebElementFacade txtIP;
//	Campo Fecha
	@FindBy(xpath="//*[@id='date3']")
	public WebElementFacade txtDate1;
//	Campo Date Earlier
	@FindBy(xpath="//*[@id='past']") 
	public WebElementFacade txtDate2;
//	Boton Validate
	@FindBy(xpath="//*[@id='popup-validation']/div[14]/input")
	public WebElementFacade btnValidate;
	
//	Globo Informativo
	@FindBy(xpath="(//DIV[@class='formErrorContent'])[1]")
	public WebElementFacade globoInformativo;
	
	
	public void required(String datoPrueba) {
		txtRequired.click();
		txtRequired.clear();
		txtRequired.sendKeys(datoPrueba);
	}
	
	public void select_Sport(String datoPrueba) {
		cmbSport1.click();
		cmbSport1.selectByVisibleText(datoPrueba);
	}
	
	public void Multiple_Select(String datoPrueba) {
		cmbSport2.selectByVisibleText(datoPrueba);
	}
	
	public void Url(String datoPrueba) {
		txtUrl.click();
		txtUrl.clear();
		txtUrl.sendKeys(datoPrueba);
	}
	
	public void Email(String datoPrueba) {
		txtEmail.click();
		txtEmail.clear();
		txtEmail.sendKeys(datoPrueba);
	}
	
	public void Password1(String datoPrueba) {
		txtPass1.click();
		txtPass1.clear();
		txtPass1.sendKeys(datoPrueba);
	}
	
	public void Password2(String datoPrueba) {
		txtPass2.click();
		txtPass2.clear();
		txtPass2.sendKeys(datoPrueba);
	}
	
	public void MiniSize(String datoPrueba) {
		txtMinimun6.click();
		txtMinimun6.clear();
		txtMinimun6.sendKeys(datoPrueba);
	}
	
	public void MaxSize(String datoPrueba) {
		txtMaximunSize.click();
		txtMaximunSize.clear();
		txtMaximunSize.sendKeys(datoPrueba);
	}
	
	public void Numero(String datoPrueba) {
		txtNumber.click();
		txtNumber.clear();
		txtNumber.sendKeys(datoPrueba);
	}
	
	public void Ip(String datoPrueba) {
		txtIP.click();
		txtIP.clear();
		txtIP.sendKeys(datoPrueba);
	}
	
	public void Fecha(String datoPrueba) {
		txtDate1.click();
		txtDate1.clear();
		txtDate1.sendKeys(datoPrueba);
	}
	
	public void FechaEarlier(String datoPrueba) {
		txtDate2.click();
		txtDate2.clear();
		txtDate2.sendKeys(datoPrueba);
	}
	
	public void validate() {
		btnValidate.click();
	}
	
	public void form_sin_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(false));
	}
	public void form_con_errores() {
		assertThat(globoInformativo.isCurrentlyVisible(), is(true));
	}
	
	
}
